%% Add sub-side effect map Rev05 (jumped revs 1-4 to be in line with main script
% check whether training files for effect threshold also exist in sub
% side effect folder
% Rev 07, Sept.2018, incorporating false discovery rate

% SSE - sub side effect
filesLeftSSE = [];
filesRightSSE = [];
for fIdx = 1:size(filesLeft,1)
    refString = filesLeft(fIdx).name;
    % does the simulation exist also in sub side effect folder?
    if exist(['leftSubSideEffect\' refString],'file') == 2
        filesLeftSSE = [filesLeftSSE; filesLeft(fIdx)];
    end
end
for fIdx = 1:size(filesRight,1)
    refString = filesRight(fIdx).name;
    % does the simulation exist also in sub side effect folder?
    if exist(['rightSubSideEffect\' refString],'file') == 2
        filesRightSSE = [filesRightSSE; filesRight(fIdx)];
    end
end

imageSizeSSE = 180; % Rev 07C increased from 180 to accomodate all files
noVTAsSSE = size(filesLeftSSE,1) + size(filesRightSSE,1);
singleClinicalScoresSSE = zeros(imageSizeSSE,imageSizeSSE,imageSizeSSE,noVTAsSSE); % for calculating p-image
globalVTASSE = zeros(imageSizeSSE,imageSizeSSE,imageSizeSSE);
pValuesPooledSSE = ones(imageSizeSSE,imageSizeSSE,imageSizeSSE);
% rev 7 q for false discuvery rate, see Genovese et al. (2002)
qFDRSSE = 0.05; % allow 33% here
numberOfVoxelsSSE = imageSizeSSE^3;
constantV = log(numberOfVoxelsSSE) + 0.5772;
pValuesPooledSSEBound = ((1:numberOfVoxelsSSE)/numberOfVoxelsSSE * qFDRSSE/constantV)';
counterThresholdRightSSE = 0;
counterThresholdLeftSSE = 0;
disp('Sub side effect map')
tic
for fIdx = 1:size(filesRightSSE,1)
        counterThresholdRightSSE = counterThresholdRightSSE + 1;
        nii = ea_load_nii(['rightSubSideEffect/' filesRightSSE(fIdx).name]);
        tmpNiiRight = zeros(imageSizeSSE,imageSizeSSE,imageSizeSSE);
        if ~exist('niiRightSSE','var')
            niiRightSSE = nii;
            niiRightSSE.img = zeros(imageSizeSSE,imageSizeSSE,imageSizeSSE);
        end
        nii.img(isnan(nii.img))=0;
        nii.img = round(nii.img);
        
        % map from current nii to base right image niiRight
        [xx,yy,zz]=ind2sub(size(nii.img),find(nii.img>0)); % find 3D-points that have correct value.
        if ~isempty(xx)
            XYZ=[xx,yy,zz]; % concatenate points to one matrix.
            XYZ=map_coords_proxy(XYZ,nii); % map to mm-space
            XYZCorrectedOrig = map_proxy_coords(XYZ,niiRightSSE); % map from mm space to index of base image
            XYZCorrectedMM = map_coords_proxy(XYZCorrectedOrig,niiRightSSE);
            xxCorrected = XYZCorrectedOrig(:,1); yyCorrected = XYZCorrectedOrig(:,2); zzCorrected = XYZCorrectedOrig(:,3);
            idxSSE = sub2ind(size(niiRightSSE.img),xxCorrected,yyCorrected,zzCorrected); % get the linear index for the base image niiRight
            tmpNiiRight(idxSSE) = 1;
            
            niiRightSSE.img = niiRightSSE.img + tmpNiiRight;
            singleClinicalScoresSSE(:,:,:,counterThresholdRightSSE) = tmpNiiRight;
            globalVTASSE = globalVTASSE + tmpNiiRight;
        end
end
globalClinicalScoreSSE = niiRightSSE.img;

for fIdx = 1:size(filesLeftSSE,1)
        counterThresholdLeftSSE = counterThresholdLeftSSE + 1;
        nii = ea_load_nii(['leftSubSideEffect/' filesLeft(fIdx).name]);
        tmpNiiRight = zeros(imageSizeSSE,imageSizeSSE,imageSizeSSE);
        tmpNiiLeft = zeros(imageSizeSSE,imageSizeSSE,imageSizeSSE);
        if ~exist('niiLeftSSE','var')
            niiLeftSSE = nii;
            niiLeftSSE.img = zeros(imageSizeSSE,imageSizeSSE,imageSizeSSE);
        end
        nii.img(isnan(nii.img))=0;
        nii.img = round(nii.img);
        
        [xx,yy,zz]=ind2sub(size(nii.img),find(nii.img>0)); % find 3D-points that have correct value.
        if ~isempty(xx)
            XYZ=[xx,yy,zz]; % concatenate points to one matrix.
            XYZ=map_coords_proxy(XYZ,nii); % map to mm-space for that nii file
            
            % for pooled data
            XYZflipped = ea_flip_lr_nonlinear(XYZ);
            tmpFlipped = map_proxy_coords(XYZflipped,niiRightSSE);
            XYZFlippedMM = map_coords_proxy(tmpFlipped,niiRightSSE);
            xxFlipped = tmpFlipped(:,1); yyFlipped = tmpFlipped(:,2); zzFlipped = tmpFlipped(:,3);
            idxSSE = sub2ind(size(niiRightSSE.img),xxFlipped,yyFlipped,zzFlipped);
            tmpNiiRight(idxSSE) = 1;
            globalClinicalScoreSSE = globalClinicalScoreSSE + tmpNiiRight;
            singleClinicalScoresSSE(:,:,:,counterThresholdRightSSE+counterThresholdLeftSSE) = tmpNiiRight;
            globalVTASSE = globalVTASSE + tmpNiiRight;
        end
end

% averaging
globalCutOff = 1;
idxVTACutoff = find(globalVTASSE < globalCutOff);
globalClinicalScoreSSE(idxVTACutoff) = 0;

tmpIdx = find(globalVTASSE >= globalCutOff);
globalClinicalScoreSSE(tmpIdx) = globalClinicalScoreSSE(tmpIdx)./(counterThresholdLeftSSE+counterThresholdRightSSE); %globalVTA(tmpIdx);
% averaging end

% p-value computation
singleClinicalScoresSSE(:,:,:,counterThresholdRightSSE+counterThresholdLeftSSE+1:end) = [];

for xIdx = 1:imageSizeSSE
    for yIdx = 1:imageSizeSSE
        for zIdx = 1:imageSizeSSE
            sampleVector = squeeze(singleClinicalScoresSSE(xIdx,yIdx,zIdx,:));
            pValuesPooledSSE(xIdx,yIdx,zIdx) = signrank(sampleVector,0,'tail','right');
        end
    end
end
% rev 7 false discovery rate
pValuesPooledSSESorted = sort(pValuesPooledSSE(:));
rFDRSSE = find(pValuesPooledSSESorted > pValuesPooledSSEBound,1,'first');
% pValuesPooledSSE = reshape(pValuesPooledSSESorted,imageSizeSSE,imageSizeSSE,imageSizeSSE);
% rev 7 ends

idxTmpSSE = find(pValuesPooledSSE >= pValuesPooledSSESorted(rFDRSSE)); % before rev 07 was >0.05
[xx,yy,zz]=ind2sub(size(pValuesPooledSSE),idxTmpSSE); 
if ~isempty(xx)
    XYZ=[xx,yy,zz]; % concatenate points to one matrix.
    XYZ=map_coords_proxy(XYZ,niiRightSSE); % map to mm-space
    
    XYZCorrectedOrig = map_proxy_coords(XYZ,niiRight); % map from mm space to index of base image
    XYZCorrected = zeros(size(XYZCorrectedOrig));
    counterSSE = 0;
    for k = 1:size(XYZCorrectedOrig,1)
        if (XYZCorrectedOrig(k,:)<=imageSize) & (XYZCorrectedOrig(k,:)>1)
            counterSSE = counterSSE + 1;
            XYZCorrected(counterSSE,:) = XYZCorrectedOrig(k,:);
        end
    end
    XYZCorrected(counterSSE+1:end,:) = []; % delete empty rows
    xxCorrected = XYZCorrected(:,1); yyCorrected = XYZCorrected(:,2); zzCorrected = XYZCorrected(:,3);
    idxSSE = sub2ind(size(niiRight.img),xxCorrected,yyCorrected,zzCorrected); % get the linear index for the base image niiRight    
end
toc

debug = true;
if debug
    fhSafeZone = figure('color','w');
    hold on
    trisurf(boundarySTNRight,ptCloud.Location((Idx & IdxRight),1),ptCloud.Location((Idx & IdxRight),2),ptCloud.Location((Idx & IdxRight),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.2,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
    xlabel('Medial-lateral (mm)')
    ylabel('Posterior-anterior (mm)')
    zlabel('Inferior-superior (mm)')

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% in-line functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function coords=map_coords_proxy(XYZ,V)

XYZ=[XYZ';ones(1,size(XYZ,1))];

coords=V.mat*XYZ;
coords=coords(1:3,:)';
end

function XYZ=map_proxy_coords(coords,V)

coords = [coords';ones(1,size(coords,1))];
XYZ = V.mat\coords;
XYZ = round(XYZ);
XYZ(XYZ<1) = 1;
XYZ = XYZ(1:3,:)';
end