% Probabilistic stimulation map
% Rev 1, go through all directional contacts
% Rev 2, all directional contacts, debugged
% Rev 2b, look at most effective contacts, ie. full effect at <= 2mA
% Rev 03, June 26, 2018, after discussion with Andreas about p-image etc;
% keep separate p-images for left and right hemispheres
% Rev 04, doing pooled map with k-fold cross-validation, split data set into k blocs
% train on four blocs and test correlation on fifth,
% Rev 05 July 16th, 2018, include the sub-side effect threshold
% Rev 06, July 19th, 2018, production figures
% Rev 07, Sept 6th, 2018, incorporate false discovery rate from Genovese et
% al. (2002) and Logan and Rowe (2004); will change p-image 

close all
clearvars

filesLeftFull = dir('left');
filesLeftFull(1:2) = [];
filesRightFull = dir('right');
filesRightFull(1:2) = [];
effectThresholdsRev02;
clinicalScoresRev02;
markerSize = 6;
printImages = false;
numberFolds = 5; % number of cross-validation
fh1 = figure('color','w'); % pooled n-image
fh3 = figure('color','w'); % p-image
fh4 = figure('color','w'); % n-p image
fileAtlas = 'anatomyDISTAL.ply';
colorIdx = 13; % color code for STN in DISTAL atlas
ptCloud = pcread(fileAtlas);
pcColors = unique(ptCloud.Color,'rows');
tmpIdx = ptCloud.Color == pcColors(colorIdx,:);
Idx = sum(tmpIdx,2) == 3;
IdxLeft = ptCloud.Location(:,1)<0;
IdxRight = ptCloud.Location(:,1)>=0;
[boundarySTNLeft,~] = boundary(double(ptCloud.Location((Idx & IdxLeft),1)),double(ptCloud.Location((Idx & IdxLeft),2)),double(ptCloud.Location((Idx & IdxLeft),3)),0.25);
[boundarySTNRight,~] = boundary(double(ptCloud.Location((Idx & IdxRight),1)),double(ptCloud.Location((Idx & IdxRight),2)),double(ptCloud.Location((Idx & IdxRight),3)),0.25);
colorOrder = [... % default color order
    0    0.4470    0.7410;...
    0.8500    0.3250    0.0980;...
    0.9290    0.6940    0.1250;...
    0.4940    0.1840    0.5560;...
    0.4660    0.6740    0.1880;...
    0.3010    0.7450    0.9330;...
    0.6350    0.0780    0.1840]; 
colorBrewer = 1/255*[...
    166 97 26;...
    223 194 125;...
    128 205 193;...
    1 133 113];
baseFigureFolder = 'rev07/';

% modify filesLeft and filesRight to split into training and testing
rng('default')
rLeft = randperm(size(filesLeftFull,1)); % random shuffle
rRight = randperm(size(filesRightFull,1));
blocSizeLeft = floor(size(filesLeftFull,1)/numberFolds);
blocSizeRight = floor(size(filesRightFull,1)/numberFolds);
pValueBound = zeros(numberFolds,1); % for False Discovery Rate

fix(clock)
for kCrossValid=1:1
    % for testing
    filesLeftTesting = filesLeftFull(rLeft((kCrossValid-1)*blocSizeLeft+1:(kCrossValid)*blocSizeLeft),:);
    filesRightTesting = filesRightFull(rRight((kCrossValid-1)*blocSizeRight+1:(kCrossValid)*blocSizeRight),:);
    % for training
    idxFileLeftTesting = (kCrossValid-1)*blocSizeLeft+1:(kCrossValid)*blocSizeLeft;
    idxFileRightTesting = (kCrossValid-1)*blocSizeRight+1:(kCrossValid)*blocSizeRight;
    idxFileLeftTraining = setdiff(1:size(filesLeftFull,1),idxFileLeftTesting);
    idxFileRightTraining = setdiff(1:size(filesRightFull,1),idxFileRightTesting);
    filesLeft = filesLeftFull(rLeft(idxFileLeftTraining)); % use the previous revisions and pass along the files for training
    filesRight = filesRightFull(rRight(idxFileRightTraining));
    
    figure(fh1)
    hold off
%     trisurf(boundarySTNLeft,ptCloud.Location((Idx & IdxLeft),1),ptCloud.Location((Idx & IdxLeft),2),ptCloud.Location((Idx & IdxLeft),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.1,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
    hold on
    trisurf(boundarySTNRight,ptCloud.Location((Idx & IdxRight),1),ptCloud.Location((Idx & IdxRight),2),ptCloud.Location((Idx & IdxRight),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.2,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
    xlabel('Medial-lateral (mm)')
    ylabel('Posterior-anterior (mm)')
    zlabel('Inferior-superior (mm)')
    grid on
    xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
        
    figure(fh3)
    hold off
%     trisurf(boundarySTNLeft,ptCloud.Location((Idx & IdxLeft),1),ptCloud.Location((Idx & IdxLeft),2),ptCloud.Location((Idx & IdxLeft),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.1,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
    hold on
    trisurf(boundarySTNRight,ptCloud.Location((Idx & IdxRight),1),ptCloud.Location((Idx & IdxRight),2),ptCloud.Location((Idx & IdxRight),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.2,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
    xlabel('Medial-lateral (mm)')
    ylabel('Posterior-anterior (mm)')
    zlabel('Inferior-superior (mm)')
    grid on
    xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
    
    figure(fh4)
    hold off
%     trisurf(boundarySTNLeft,ptCloud.Location((Idx & IdxLeft),1),ptCloud.Location((Idx & IdxLeft),2),ptCloud.Location((Idx & IdxLeft),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.1,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
    hold on
    trisurf(boundarySTNRight,ptCloud.Location((Idx & IdxRight),1),ptCloud.Location((Idx & IdxRight),2),ptCloud.Location((Idx & IdxRight),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.2,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
    xlabel('Medial-lateral (mm)')
    ylabel('Posterior-anterior (mm)')
    zlabel('Inferior-superior (mm)')
    grid on
    xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
        
    %% building indices
    % Contact indices
    
    idxLeft = zeros(size(filesLeft,1),2);
    for fIdx=1:size(filesLeft)
        tmp = filesLeft(fIdx).name;
        pNumber = str2double(tmp(8:10));
        cNumber = str2double(tmp(12:13))-7; % -7 for conversion to neurology numbering
        idxLeft(fIdx,:) = [pNumber cNumber];
    end
    
    idxRight = zeros(size(filesRight,1),2);
    for fIdx=1:size(filesRight)
        tmp = filesRight(fIdx).name;
        pNumber = str2double(tmp(8:10));
        cNumber = str2double(tmp(12))+9; % +9 for conversion to neurology numbering
        idxRight(fIdx,:) = [pNumber cNumber];
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% basic data generation and pooled n-image
    if kCrossValid > 1
        clear globalVTA VTAsLeft VTAsRight singleClinicalScore pValuesPooledBound ...
            pValuesLeft pValuesRight pValuesPooled
    end
    imageSize = 140;
    noVTAs = size(idxLeft,1) + size(idxRight,1);
    globalVTA = zeros(imageSize,imageSize,imageSize); % counting the number of VTAs that have that voxel activated
    VTAsLeft = zeros(imageSize,imageSize,imageSize);
    VTAsRight = zeros(imageSize,imageSize,imageSize);
    singleClinicalScores = zeros(imageSize,imageSize,imageSize,noVTAs); % for calculating p-image
    % rev 7, q for false discuvery rate, see Genovese et al. (2002)
    qFDR = 0.05;
    numberOfVoxels = imageSize^3;
    constantV = log(numberOfVoxels) + 0.5772; 
    pValuesPooledBound = ((1:numberOfVoxels)/numberOfVoxels * qFDR/constantV)';
    pValuesLeft = ones(imageSize,imageSize,imageSize);
    pValuesRight = ones(imageSize,imageSize,imageSize);
    pValuesPooled = ones(imageSize,imageSize,imageSize);
    counterThresholdRight = 0;
    counterThresholdLeft = 0;
    disp('Map training')
    tic
    for fIdx = 1:length(filesRight)
        if (effectThreshold(idxRight(fIdx,1),idxRight(fIdx,2)) > 0) && (clinicalScore(idxRight(fIdx,1),idxRight(fIdx,2)) >= 0) % set conditions
            counterThresholdRight = counterThresholdRight + 1;
            nii = ea_load_nii(['right/' filesRight(fIdx).name]);
            tmpNiiRight = zeros(imageSize,imageSize,imageSize);
            if ~exist('niiRight','var')
                niiRight = nii;
                niiRight.img = zeros(imageSize,imageSize,imageSize);
            end
            nii.img(isnan(nii.img))=0;
            nii.img = round(nii.img);
            
            % map from current nii to base right image niiRight
            [xx,yy,zz]=ind2sub(size(nii.img),find(nii.img>0)); % find 3D-points that have correct value.
            if ~isempty(xx)
                XYZ=[xx,yy,zz]; % concatenate points to one matrix.
                XYZ=map_coords_proxy(XYZ,nii); % map to mm-space
                XYZCorrected = map_proxy_coords(XYZ,niiRight); % map from mm space to index of base image
                XYZCorrectedMM = map_coords_proxy(XYZCorrected,niiRight);
                xxCorrected = XYZCorrected(:,1); yyCorrected = XYZCorrected(:,2); zzCorrected = XYZCorrected(:,3);
                linearIdx = sub2ind(size(niiRight.img),xxCorrected,yyCorrected,zzCorrected); % get the linear index for the base image niiRight
                tmpNiiRight(linearIdx) = 1;
                
                niiRight.img = niiRight.img + tmpNiiRight.*clinicalScoreNormalized(idxRight(fIdx,1),idxRight(fIdx,2));
                singleClinicalScores(:,:,:,counterThresholdRight) = tmpNiiRight.*clinicalScoreNormalized(idxRight(fIdx,1),idxRight(fIdx,2)); 
                globalVTA = globalVTA + tmpNiiRight;
                VTAsRight = VTAsRight + tmpNiiRight;
            end
        end
    end
    globalClinicalScore = niiRight.img;
    
    for fIdx = 1:length(filesLeft)
        if (effectThreshold(idxLeft(fIdx,1),idxLeft(fIdx,2)) > 0) && (clinicalScore(idxLeft(fIdx,1),idxLeft(fIdx,2)) >= 0)
            counterThresholdLeft = counterThresholdLeft + 1;
            nii = ea_load_nii(['left/' filesLeft(fIdx).name]);
            tmpNiiRight = zeros(imageSize,imageSize,imageSize);
            tmpNiiLeft = zeros(imageSize,imageSize,imageSize);
            if ~exist('niiLeft','var')
                niiLeft = nii;
                niiLeft.img = zeros(imageSize,imageSize,imageSize);
            end
            nii.img(isnan(nii.img))=0;
            nii.img = round(nii.img);
            
            [xx,yy,zz]=ind2sub(size(nii.img),find(nii.img>0)); % find 3D-points that have correct value.
            if ~isempty(xx)
                XYZ=[xx,yy,zz]; % concatenate points to one matrix.
                XYZ=map_coords_proxy(XYZ,nii); % map to mm-space for that nii file
                
                % for pooled data
                XYZflipped = ea_flip_lr_nonlinear(XYZ);
                tmpFlipped = map_proxy_coords(XYZflipped,niiRight);
                XYZFlippedMM = map_coords_proxy(tmpFlipped,niiRight);
                xxFlipped = tmpFlipped(:,1); yyFlipped = tmpFlipped(:,2); zzFlipped = tmpFlipped(:,3);
                linearIdx = sub2ind(size(niiRight.img),xxFlipped,yyFlipped,zzFlipped);
                tmpNiiRight(linearIdx) = 1;
                globalClinicalScore = globalClinicalScore + tmpNiiRight.*clinicalScoreNormalized(idxLeft(fIdx,1),idxLeft(fIdx,2));
                singleClinicalScores(:,:,:,counterThresholdRight+counterThresholdLeft) = tmpNiiRight.*clinicalScoreNormalized(idxLeft(fIdx,1),idxLeft(fIdx,2));
                globalVTA = globalVTA + tmpNiiRight;
                
            end
        end
    end
    % averaging
    globalCutOff = 1;
    idxVTACutoff = find(globalVTA < globalCutOff);
    globalClinicalScore(idxVTACutoff) = 0;
    
    tmpIdx = find(globalVTA >= globalCutOff);
    globalClinicalScore(tmpIdx) = globalClinicalScore(tmpIdx)./(counterThresholdLeft+counterThresholdRight); %globalVTA(tmpIdx);
    % averaging end
    
    % plot
    tmpIdx = find(globalClinicalScore>0);
    [xx,yy,zz]=ind2sub(size(globalClinicalScore),tmpIdx); % find 3D-points that have correct value.
    if ~isempty(xx)
        XYZ=[xx,yy,zz]; % concatenate points to one matrix.
        XYZ=map_coords_proxy(XYZ,niiRight); % map to mm-space
    end
    figure(fh1)
    h = scatter3(XYZ(1:25:end,1),XYZ(1:25:end,2),XYZ(1:25:end,3),markerSize,globalClinicalScore(tmpIdx(1:25:end)),'filled');
    h.MarkerFaceAlpha = 0.4;
    
    view([-30,30])
    cb = colorbar;
    caxis([0 max(globalClinicalScore(:))])
    cb.Label.String = 'Mean clinical efficacy';
    title('Mean clinical efficacy image')
    
    %% p-image
    figure(fh3)
    % delete empty columns
    singleClinicalScores(:,:,:,counterThresholdRight+counterThresholdLeft+1:end) = [];
    % do the signrank test for each voxel
        
    % p-values pooled 
%     if kCrossValid >1
    for xIdx = 1:imageSize
        for yIdx = 1:imageSize
            for zIdx = 1:imageSize
                sampleVector = squeeze(singleClinicalScores(xIdx,yIdx,zIdx,:));
                pValuesPooled(xIdx,yIdx,zIdx) = signrank(sampleVector,0,'tail','right');
            end
        end
    end
    % rev 7 false discovery rate
    pValuesPooledSorted = sort(pValuesPooled(:));
    rFDR = find(pValuesPooledSorted > pValuesPooledBound,1,'first');
    pValueBound(kCrossValid) = pValuesPooledSorted(rFDR-1);
    toc
    tmpIdx = find(pValuesPooled > 0 & pValuesPooled < 1);
    % rev 7 ends
    [xx,yy,zz]=ind2sub(size(pValuesPooled),tmpIdx); % find 3D-points that have correct value.
    if ~isempty(xx)
        XYZ=[xx,yy,zz]; % concatenate points to one matrix.
        XYZ=map_coords_proxy(XYZ,niiRight); % map to mm-space
    end
    h = scatter3(XYZ(1:25:end,1),XYZ(1:25:end,2),XYZ(1:25:end,3),markerSize,pValuesPooled(tmpIdx(1:25:end)),'filled');
    h.MarkerFaceAlpha = 0.4;
    
    view([-30,30])
    xlim([0 20])
    cb = colorbar;
    caxis([0 1])
    cb.Label.String = 'p-values';
    title('p-image')
%     end  
    %% add sub-side effect map
    probabilisticStimulationMapSubSideEffectRev07;
    globalClinicalScore(idxSSE) = -1; % mark side effect voxels, also tried -inf, but maybe too harsh a penalty
    %% n-image and p-image combined
    figure(fh4)
    tmpIdx = find(pValuesPooled <= pValueBound(kCrossValid));
    [xx,yy,zz]=ind2sub(size(pValuesPooled),tmpIdx); % find 3D-points that have correct value.
    if ~isempty(xx)
        XYZ=[xx,yy,zz]; % concatenate points to one matrix.
        XYZ=map_coords_proxy(XYZ,niiRight); % map to mm-space
    end
    h = scatter3(XYZ(1:10:end,1),XYZ(1:10:end,2),XYZ(1:10:end,3),markerSize,globalClinicalScore(tmpIdx(1:10:end)),'filled');
    h.MarkerFaceAlpha = 0.4;

    view([-30,30])
    xlim([0 20])
    cb = colorbar;
    caxis([0 max(globalClinicalScore(:))])
    cb.Label.String = 'Mean clinical efficacy';
    title('Significant mean clinical efficacy image')
            
    %% Testing
    probabilisticStimulationMapTestingRev07;
%     if kCrossValid == 1
%         input('Any key to continue')
%     end
    
    %% image saving
    if printImages
        figure(fh1)
        view([-30 30])
        saveas(fh1,[baseFigureFolder 'nImagePooledAK' num2str(kCrossValid)],'png')
%         print(fh1,[baseFigureFolder 'nImagePooledA'],'-dtiff','-r300')
        print(fh1,[baseFigureFolder 'nImagePooledAK' num2str(kCrossValid)],'-depsc')
%         view([30 30])
%         saveas(fh1,[baseFigureFolder 'nImagePooledA2K' num2str(kCrossValid)],'png')
%         view([90 0])
%         saveas(fh1,[baseFigureFolder 'nImagePooledBK' num2str(kCrossValid)],'png')
%         view([0 0])
%         saveas(fh1,[baseFigureFolder 'nImagePooledCK' num2str(kCrossValid)],'png')
        title('')
        view([0 90])
        saveas(fh1,[baseFigureFolder 'nImagePooledDK' num2str(kCrossValid)],'png')
%         print(fh1,[baseFigureFolder 'nImagePooledD'],'-dtiff','-r300')
        print(fh1,[baseFigureFolder 'nImagePooledDK' num2str(kCrossValid)],'-depsc')
              
        figure(fh3)
        view([-30 30])
        saveas(fh3,[baseFigureFolder 'pImagePooledAK' num2str(kCrossValid)],'png')
%         print(fh3,[baseFigureFolder 'pImagePooledA'],'-dtiff','-r300')
        print(fh3,[baseFigureFolder 'pImagePooledAK' num2str(kCrossValid)],'-depsc')
%         view([30 30])
%         saveas(fh3,[baseFigureFolder 'pImagePooledA2'],'png')
%         view([90 0])
%         saveas(fh3,[baseFigureFolder 'pImagePooledB'],'png')
%         view([0 0])
%         saveas(fh3,[baseFigureFolder 'pImagePooledC'],'png')
        title('')
        view([0 90])
        saveas(fh3,[baseFigureFolder 'pImagePooledDK' num2str(kCrossValid)],'png')
%         print(fh3,[baseFigureFolder 'pImagePooledD'],'-dtiff','-r300')
        print(fh3,[baseFigureFolder 'pImagePooledDK' num2str(kCrossValid)],'-depsc')
        
        figure(fh4)
        view([-30 30])
        saveas(fh4,[baseFigureFolder 'npImagePooledAK' num2str(kCrossValid)],'png')
%         print(fh4,[baseFigureFolder 'npImagePooledA'],'-dtiff','-r300')
        print(fh4,[baseFigureFolder 'npImagePooledAK' num2str(kCrossValid)],'-depsc')
%         view([30 30])
%         saveas(fh4,[baseFigureFolder 'npImagePooledA2'],'png')
%         view([90 0])
%         saveas(fh4,[baseFigureFolder 'npImagePooledB'],'png')
%         view([0 0])
%         saveas(fh4,[baseFigureFolder 'npImagePooledC'],'png')
        title('')
        view([0 90])
        saveas(fh4,[baseFigureFolder 'npImagePooledDK' num2str(kCrossValid)],'png')
%         print(fh4,[baseFigureFolder 'npImagePooledD'],'-dtiff','-r300')
        print(fh4,[baseFigureFolder 'npImagePooledDK' num2str(kCrossValid)],'-depsc')
                
        saveas(fh1,[baseFigureFolder 'nImagePooledK' num2str(kCrossValid)],'fig')
        saveas(fh3,[baseFigureFolder 'pImagePooledK' num2str(kCrossValid)],'fig')
        saveas(fh4,[baseFigureFolder 'npImagePooledK' num2str(kCrossValid)],'fig')
        
    end
    switch kCrossValid
        case 1
            save('PSMFromCrossValidK1Rev07.mat','globalClinicalScore','niiRight','pValuesPooled');
        case 2
            save('PSMFromCrossValidK2Rev07.mat','globalClinicalScore','niiRight','pValuesPooled');
        case 3
            save('PSMFromCrossValidK3Rev07.mat','globalClinicalScore','niiRight','pValuesPooled');
        case 4
            save('PSMFromCrossValidK4Rev07.mat','globalClinicalScore','niiRight','pValuesPooled');
        case 5
            save('PSMFromCrossValidK5Rev07.mat','globalClinicalScore','niiRight','pValuesPooled');
    end
end % for kCrossValid
fix(clock)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% in-line functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function coords=map_coords_proxy(XYZ,V)

XYZ=[XYZ';ones(1,size(XYZ,1))];

coords=V.mat*XYZ;
coords=coords(1:3,:)';
end

function XYZ=map_proxy_coords(coords,V)

coords = [coords';ones(1,size(coords,1))];
XYZ = V.mat\coords;
XYZ = round(XYZ);
XYZ(XYZ<1) = 1;
XYZ = XYZ(1:3,:)';
end
