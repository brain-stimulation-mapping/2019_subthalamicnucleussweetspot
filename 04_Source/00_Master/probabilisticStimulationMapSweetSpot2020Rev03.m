% Computing probabilistic stimulation map 2020
% Purpose: update map with DiODe orientation detection and better knowledge
% Perform sweet spot generation, no cross validation
% Rev 01, June 2020, thuyanhkhoa.nguyen@insel.ch; starting from
% probabilisticStimulationMapRev07.m
% Rev 02, Jan 2021, averaging voxel scores by n-image, not by total VTA number
% Rev 03, Feb 2021, implementing Reich's statistical test

close all
clearvars

%% Figure handling
markerSize = 6;
printImages = false;
plotImages = true;
fileAtlas = ...
    'anatomyDISTALminimal.ply';
colorIdx = 4; % color code for STN in DISTAL atlas
ptCloud = pcread( fileAtlas );
pcColors = unique( ptCloud.Color,'rows' );
tmpIdx = ptCloud.Color == pcColors( colorIdx,: );
Idx = sum( tmpIdx,2 ) == 3;
IdxLeft = ptCloud.Location(:,1) < 0;
IdxRight = ptCloud.Location(:,1) >= 0;
[ boundarySTNLeft, ~ ] = boundary( ...
    double(ptCloud.Location((Idx & IdxLeft),1)), ...
    double(ptCloud.Location((Idx & IdxLeft),2)), ...
    double(ptCloud.Location((Idx & IdxLeft),3)),0.25);
[ boundarySTNRight, ~ ] = boundary( ...
    double(ptCloud.Location((Idx & IdxRight),1)), ...
    double(ptCloud.Location((Idx & IdxRight),2)), ...
    double(ptCloud.Location((Idx & IdxRight),3)),0.25);
baseFigureFolder = ...
    '';

%% Setup
effectThresholdsRev03;
numberFolds = 1; % number of cross-validation
labelContacts = {'01','02','03','04','05','06','07','08','234','567',...
    '09','10','11','12','13','14','15','16','101112','131415'};

listAllFiles = dir(...
    '/reslicedDirectionalBrainStim/' );
listAllFiles(1:2) = [];
rng('default')
randomizedFileIndex = randperm(size(listAllFiles,1)); % random shuffle
blocSize = floor(size(listAllFiles,1)/numberFolds);

% Using MNI T1 nifti file as a container for the map
sizeMap = 100; % 20 mm in each direction
% mapContainer for effect image
mapContainer = ea_load_nii( 'anatomicalMNIT1Resliced.nii' );
mapContainer.img = zeros( sizeMap, sizeMap, sizeMap );
mapContainer.dim = [ sizeMap, sizeMap, sizeMap ];
mapContainer.mat(:,4) = [ 0; -20; -15; 1 ]; % set origin 
nImage = mapContainer;

% false discovery rate, see Genovese et al. (2002)
qFDR = 0.05;
pValueBoundCrossValidation = zeros(numberFolds,1); % for False Discovery Rate
kCrossValid = 1;

correlationRho = zeros( numberFolds, 1 );
correlationPValue = zeros( numberFolds, 1 );

%% Map generation
tic
fhMeanEfficiency = figure('color','w');
fhSignificantMeanEfficiencyBetter = figure('color','w');
fhSignificantMeanEfficiencyWorse = figure('color','w');

% refresh figures
figure( fhMeanEfficiency )
hold off
trisurf( boundarySTNRight, ptCloud.Location( ( Idx & IdxRight ), 1 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 2 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 3 ), 'Facecolor', [ .4 .4 .4 ],...
    'FaceAlpha', 0.2, 'LineStyle' , 'none' , 'EdgeColor' , pcColors( colorIdx, : ) );
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
hold on

figure( fhSignificantMeanEfficiencyBetter )
hold off
trisurf( boundarySTNRight, ptCloud.Location( ( Idx & IdxRight ), 1 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 2 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 3 ), 'Facecolor', [ .4 .4 .4 ],...
    'FaceAlpha', 0.2, 'LineStyle' , 'none' , 'EdgeColor' , pcColors( colorIdx, : ) );
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
hold on

figure( fhSignificantMeanEfficiencyWorse )
hold off
trisurf( boundarySTNRight, ptCloud.Location( ( Idx & IdxRight ), 1 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 2 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 3 ), 'Facecolor', [ .4 .4 .4 ],...
    'FaceAlpha', 0.2, 'LineStyle' , 'none' , 'EdgeColor' , pcColors( colorIdx, : ) );
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
hold on

%% Mean effect image
% file assignment for testing and training
% fix testing set, all other samples are training
%     indexFileTesting = ( kCrossValid-1 ) * blocSize + 1 : ( kCrossValid ) * blocSize;
%     indexFileTraining = setdiff( 1 : length( listAllFiles ), indexFileTesting );
%     listFilesTesting = listAllFiles( randomizedFileIndex( indexFileTesting ,: );
%     listFilesTraining = listAllFiles( randomizedFileIndex( indexFileTraining ) );
listFilesTraining = listAllFiles;

% basic data generation and pooled n-image
mapContainter.img = zeros( sizeMap, sizeMap, sizeMap );
nImage.img = zeros( sizeMap, sizeMap, sizeMap );
numberVTAs = length( listFilesTraining );
singleClinicalScores = NaN( sizeMap*sizeMap*sizeMap, numberVTAs );
counter = 0;
fTraining = waitbar( 0, sprintf( 'Map generation' ) );
for indexFileTraining = 1:length( listFilesTraining )
    waitbar( indexFileTraining/length( listFilesTraining ), fTraining, sprintf( 'Map generation' ) );
    fileNameVTA = [ listFilesTraining( indexFileTraining ).folder filesep ...
        listFilesTraining( indexFileTraining ).name ];
    tmpString = strsplit( listFilesTraining( indexFileTraining ).name, 'C');
    labelContact = tmpString{ end }( 1:end-4 ); % remove .nii
    [ ~, locb ] = ismember( labelContact, labelContacts );
    if locb < 11
        hemisphere = 'left';
    else
        hemisphere = 'right';
    end
    labelPatient = tmpString{ 1 }( 8:11 );
    
    tmpVTA = ea_load_nii( fileNameVTA );
    tmpVTA.img( isnan( tmpVTA.img )) = 0;
    tmpVTA.img = round( tmpVTA.img );
    [ xx, yy, zz ] = ind2sub( size( tmpVTA.img ), find( tmpVTA.img > 0) );
    if ~isempty( xx)
        counter = counter + 1;
        voxelCoordinatesVTA = [ xx, yy, zz ];
        worldCoordinatesVTA = mapVoxelToWorld( voxelCoordinatesVTA, tmpVTA );
        if strcmp( hemisphere, 'left' ) % flip left hemisphere VTAs
            worldCoordinatesVTA = ea_flip_lr_nonlinear(...
                worldCoordinatesVTA );
        end
        voxelCoordinatesContainer = mapWorldToVoxel( ...
            worldCoordinatesVTA, mapContainer );
        indexLinear = sub2ind( size( mapContainer.img ),...
            voxelCoordinatesContainer( :, 1 ), ...
            voxelCoordinatesContainer( :, 2 ), ...
            voxelCoordinatesContainer( :, 3 ));
        mapContainer.img( unique( indexLinear ) ) ...
            = mapContainer.img( unique( indexLinear ) ) ...
            + clinicalEfficacyTable{ labelPatient, labelContact };
        singleClinicalScores( unique( indexLinear ), counter ) = ...
            clinicalEfficacyTable{ labelPatient, labelContact };
        nImage.img( unique( indexLinear ) ) = ...
            nImage.img( unique( indexLinear ) ) + 1;
    end
end % files training
close( fTraining )
% discard voxels with insufficient activation count
activationCutOff = 15; % Dembek uses 15 so they can use approximate rank test later
voxelsToDiscard = find( nImage.img <= activationCutOff );
nImage.img( voxelsToDiscard ) = 0;
mapContainer.img( voxelsToDiscard ) = 0;
singleClinicalScores( voxelsToDiscard,: ) = ...
    NaN( numel( voxelsToDiscard ), numberVTAs );
% averaging
voxelsToAverage = find( nImage.img > 0 ); % version 2
    mapContainer.img( voxelsToAverage ) ...
        = mapContainer.img( voxelsToAverage ) ...
        ./ nImage.img( voxelsToAverage );
% mapContainer.img( voxelsToAverage ) ...
%     = mapContainer.img( voxelsToAverage ) ...
%     ./ numberVTAs;

% plot
if plotImages
    voxelsToPlot = find( nImage.img > activationCutOff );%voxelsToAverage;
    [ xx, yy, zz ] = ind2sub( size( mapContainer.img ), voxelsToPlot);
    if ~isempty( xx )
        voxelCoordinates = [ xx, yy, zz ]; % concatenate points to one matrix.
        worldCoordinates = mapVoxelToWorld( voxelCoordinates, mapContainer ); % map to mm-space
    end
    figure( fhMeanEfficiency )
    h = scatter3( ...
        worldCoordinates( :,1 ), ...
        worldCoordinates( :,2 ), ...
        worldCoordinates( :,3 ), ...
        markerSize,...
        mapContainer.img( voxelsToPlot ),'filled');
    h.MarkerFaceAlpha = 0.4;
    
    view( [ -30,30 ] )
    cb = colorbar;
    caxis( [ 0 max( mapContainer.img( voxelsToPlot ) ) ] )
    cb.Label.String = 'Mean clinical efficiency';
    title( 'Mean clinical efficiency image' )
end

%% p-Image, see Reich and Horn et al. (2019) dystonia

pValuesContainerRight = NaN( sizeMap*sizeMap*sizeMap, 1 ); % voxel mean higher than rest
pValuesContainerLeft = NaN( sizeMap*sizeMap*sizeMap, 1 ); % voxel mean lower than rest
voxelsToTest = find( nImage.img > activationCutOff );
voxelsToTest = sort( voxelsToTest );

%%%%%%% testing to accelerate, not working %%%%%%%%%%%%%%
% individualScores = NaN( numberVTAs, numel( voxelsToTest ));
% populationScores = NaN( numberVTAs*( numel( voxelsToTest ) - 1), numel( voxelsToTest ));
% for indexVoxel = 1:numel( voxelsToTest )
%     voxelsPopulation = setdiff( voxelsToTest, voxelsToTest( indexVoxel ));
%     tmp = singleClinicalScores( voxelsPopulation, : );
%     populationScores( :, indexVoxel ) = tmp(:);
%     individualScores( :, indexVoxel ) = singleClinicalScores(...
%         voxelsToTest( indexVoxel ), : );
% end
%%% working
% parfor indexVoxel = 1:size( pValuesContainerRight, 1) %voxelsToTest
%     waitbar( indexVoxel/numel( voxelsToTest ), fPImage, sprintf( 'p-image' ) );
%     if ismember( indexVoxel, voxelsToTest )
%     voxelsPopulation = setdiff( voxelsToTest, indexVoxel );
%     populationScores = singleClinicalScores( voxelsPopulation, : );
%     [ ~, pValuesContainerRight( indexVoxel )] = ...
%         ttest2( singleClinicalScores( indexVoxel, : ), ...
%         populationScores(:), ...
%         'alpha', 0.05, 'tail', 'right');
%     [ ~, pValuesContainerLeft( voxelsToTest( indexVoxel ) )] = ...
%         ttest2( singleClinicalScores( voxelsToTest( indexVoxel ), : )', ...
%         populationScores(:), ...
%         'alpha', 0.05, 'tail', 'left');
%     clearvars populationScores
%     end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parfor indexVoxel = 1:size( pValuesContainerRight, 1)
    if ismember( indexVoxel, voxelsToTest )
        voxelsPopulation = setdiff( voxelsToTest, indexVoxel );
        populationScores = singleClinicalScores( voxelsPopulation, : );
        [ ~, pValuesContainerRight( indexVoxel )] = ...
            ttest2( singleClinicalScores( indexVoxel, : ), ...
            populationScores(:), ...
            'alpha', 0.05, 'tail', 'right');
        [ ~, pValuesContainerLeft( indexVoxel )] = ...
            ttest2( singleClinicalScores( indexVoxel , : ), ...
            populationScores(:), ...
            'alpha', 0.05, 'tail', 'left');
    end
end

% false discovery rate, see Genovese et al. (2002)
qFDR = 0.05;
numberOfVoxels = numel( voxelsToTest );
constantV = log( numberOfVoxels ) + 0.5772;
pValuesBound = ( ( 1:numberOfVoxels )/numberOfVoxels * qFDR/constantV )';

pValuesSortedRight = sort( pValuesContainerRight );
pValuesSortedRight = pValuesSortedRight( ~isnan( pValuesSortedRight ) );
rFDR = find( pValuesSortedRight > pValuesBound, 1, 'first' );
pValueBoundRight = pValuesSortedRight(rFDR-1);
pValuesSortedLeft = sort( pValuesContainerLeft );
pValuesSortedLeft = pValuesSortedLeft( ~isnan( pValuesSortedLeft ) );
rFDR = find( pValuesSortedLeft > pValuesBound, 1, 'first' );
pValueBoundLeft = pValuesSortedLeft(rFDR-1);

% use these two lines for original Reich method
% pValueBoundRight = 0.05;
% pValueBoundLeft = 0.05;

mapSignificantEfficiencyBetter = mapContainer;
voxelsToDiscard = find( pValuesContainerRight >= pValueBoundRight );
mapSignificantEfficiencyBetter.img( voxelsToDiscard ) = 0;

if plotImages
    voxelsToPlot = find( pValuesContainerRight < pValueBoundRight );
    [ xx, yy, zz] = ind2sub( size( mapContainer.img ), voxelsToPlot );
    if ~isempty( xx )
        voxelCoordinates = [ xx, yy, zz ]; % concatenate points to one matrix.
        worldCoordinates = mapVoxelToWorld( voxelCoordinates, mapContainer ); % map to mm-space
    end
    figure( fhSignificantMeanEfficiencyBetter )
    h = scatter3( ...
        worldCoordinates( :,1 ), ...
        worldCoordinates( :,2 ), ...
        worldCoordinates( :,3 ), ...
        markerSize,...
        mapContainer.img( voxelsToPlot ),'filled');
    h.MarkerFaceAlpha = 0.4;
    
    view( [ -30,30 ] )
    cb = colorbar;
    maxEfficiency = max( mapContainer.img( voxelsToPlot ) );
    caxis( [ 0 maxEfficiency ] )
    cb.Label.String = 'Mean clinical efficiency';
    title( 'Significant better mean clinical efficiency image' )
end

mapSignificantEfficiencyWorse = mapContainer;
voxelsToDiscard = find( pValuesContainerLeft >= pValueBoundLeft );
mapSignificantEfficiencyWorse.img( voxelsToDiscard ) = 0;

if plotImages
    voxelsToPlot = find( pValuesContainerLeft < pValueBoundLeft );
    [ xx, yy, zz] = ind2sub( size( mapContainer.img ), voxelsToPlot );
    if ~isempty( xx )
        voxelCoordinates = [ xx, yy, zz ]; % concatenate points to one matrix.
        worldCoordinates = mapVoxelToWorld( voxelCoordinates, mapContainer ); % map to mm-space
    end
    figure( fhSignificantMeanEfficiencyWorse )
    h = scatter3( ...
        worldCoordinates( :,1 ), ...
        worldCoordinates( :,2 ), ...
        worldCoordinates( :,3 ), ...
        markerSize,...
        mapContainer.img( voxelsToPlot ),'filled');
    h.MarkerFaceAlpha = 0.4;
    
    view( [ -30,30 ] )
    cb = colorbar;
%     caxis( [ 0 max( mapContainer.img( voxelsToPlot ) ) ] )
    caxis( [ 0 maxEfficiency ] );
    cb.Label.String = 'Mean clinical efficiency';
    title( 'Significant worse mean clinical efficiency image' )
end

%% Testing

% create sweet spot
mapSweetSpot = mapSignificantEfficiencyBetter;
voxelsSignificant = find( mapSweetSpot.img > 0 );
mapSignificantEffectBinary = mapSignificantEfficiencyBetter;
mapSignificantEffectBinary.img( voxelsSignificant ) = 1;
threshold = prctile( mapSweetSpot.img( voxelsSignificant ), 90 );
voxelsNotSweetSpot = find( mapSweetSpot.img < threshold );
voxelsSweetSpot = find( mapSweetSpot.img >= threshold );
mapSweetSpot.img( voxelsNotSweetSpot ) = 0;
mapSweetSpot.img( voxelsSweetSpot ) = 1;

%     fTesting = waitbar(0, sprintf( 'Testing K = %d', kCrossValid) );
%     overlapRatios = zeros( length( listFilesTesting ), 1 );
%     clinicalEfficacyCorrelation = zeros( length( listFilesTesting ), 1 );
%     for indexFileTesting = 1:length( listFilesTesting )
%         waitbar( indexFileTesting/length( listFilesTesting ), fTesting, sprintf( 'Testing K = %d', kCrossValid) );
%         fileNameVTA = [ listFilesTesting( indexFileTesting ).folder filesep ...
%             listFilesTesting( indexFileTesting ).name ];
%         tmpString = strsplit( listFilesTesting( indexFileTesting ).name, 'C');
%         labelContact = tmpString{ end }( 1:end-4 ); % remove .nii
%         [ ~, locb ] = ismember( labelContact, labelContacts );
%         if locb < 11
%             hemisphere = 'left';
%         else
%             hemisphere = 'right';
%         end
%         labelPatient = tmpString{ 1 }( 8:11 );
% 
%         tmpVTA = ea_load_nii( fileNameVTA );
%         tmpVTA.img( isnan( tmpVTA.img )) = 0;
%         tmpVTA.img = round( tmpVTA.img );
%         [ xx, yy, zz ] = ind2sub( size( tmpVTA.img ), find( tmpVTA.img > 0) );
%         if ~isempty( xx)
%             voxelCoordinatesVTA = [ xx, yy, zz ];
%             worldCoordinatesVTA = mapVoxelToWorld( voxelCoordinatesVTA, tmpVTA );
%             if strcmp( hemisphere, 'left' ) % flip left hemisphere VTAs
%                 worldCoordinatesVTA = ea_flip_lr_nonlinear(...
%                     worldCoordinatesVTA) ;
%             end
%             voxelCoordinatesSweetSpot = mapWorldToVoxel( ...
%                 worldCoordinatesVTA, mapSweetSpot );
%             indexLinear = sub2ind( size( mapSweetSpot.img ),...
%                 voxelCoordinatesSweetSpot( :, 1 ), ...
%                 voxelCoordinatesSweetSpot( :, 2 ), ...
%                 voxelCoordinatesSweetSpot( :, 3 ));
%             logicalVoxelsOverlap = ismember( indexLinear, ...
%                 voxelsSweetSpot );
%             overlapRatios( indexFileTesting )...
%                 = sum( logicalVoxelsOverlap )...
%                 /sum( tmpVTA.img(:) == 1 );
%             clinicalEfficacyCorrelation( indexFileTesting )...
%                 = clinicalEfficacyTable{ labelPatient, labelContact };
%         end
%     end
%     [ correlationRho( kCrossValid ), correlationPValue( kCrossValid ) ]...
%         = corr( log( overlapRatios*100 ), clinicalEfficacyCorrelation, 'type', 'Spearman');
%     close( fTesting )

toc

% tmpNifti = make_nii( mapSweetSpot.img , mapSweetSpot.voxsize, mapWorldToVoxel( [ 0 0 0 ], mapSweetSpot ) );
% save_nii(tmpNifti, '../../03_Data/sweetSpotAllRight90th2020.nii');
% tmpNifti = make_nii( mapSignificantEffect.img , mapSignificantEffect.voxsize, mapWorldToVoxel( [ 0 0 0 ], mapSignificantEffect ) );
% save_nii(tmpNifti, '../../03_Data/mapSignificantEffectAllRight.nii');
% tmpNifti = make_nii( mapSignificantEffectBinary.img , mapSignificantEffectBinary.voxsize, mapWorldToVoxel( [ 0 0 0 ], mapSignificantEffectBinary ) );
% save_nii(tmpNifti, '../../03_Data/mapSignificantEffectBinaryFDRRev03.nii');
