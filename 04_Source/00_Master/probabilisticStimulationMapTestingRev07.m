% Script for testing probabilistic stimulation map
% Rev 6 includes correlation to therapeutic window
% Rev 7 for main file rev 7
tmpIdx = find(pValuesPooled <= 0 | pValuesPooled > pValueBound(kCrossValid));
globalClinicalScore(tmpIdx) = 0; % leave the significant mean effect image
globalClinicalScore(idxSSE) = -1; % mark side effect voxels, also tried -inf, but maybe too harsh a penalty, added here in rev07!
percentileCutOff = 90;

%% contact positions and group assignment
if kCrossValid == 1 % only do on first run of cross-validation
    noPatients = 29;
    contactPositionLeft = zeros(noPatients,8,3);
    contactPositionRight = zeros(noPatients,8,3);
    colorOrder = [... % default color order
        0    0.4470    0.7410;...
        0.8500    0.3250    0.0980;...
        0.9290    0.6940    0.1250;...
        0.4940    0.1840    0.5560;...
        0.4660    0.6740    0.1880;...
        0.3010    0.7450    0.9330;...
        0.6350    0.0780    0.1840];
    
    % group assignment
    % three groups A, 0, B; A with therapeutic window of ringlevel better than
    % best direction >=1mA; 0 TW difference between -.5 and .5 mA; Group B
    % responders with best directional TW >=1mA better than ring mode
    % assignment of -1, 0, 1
    
    bestContactLeft = [...
        2 7 5;...
        2 4 3;...
        2 7 6;...
        2 7 5;...
        NaN NaN NaN;...
        2 7 6;...
        2 6 5;...
        2 7 5;...
        2 7 5;...
        2 5 6;...
        2 5 6;...
        2 6 5;...
        1 4 5;...
        1 4 2;...
        2 7 6;...
        2 6 5;...
        1 3 4;...
        2 7 5;...
        2 7 6;...
        1 3 2;...
        2 7 5;...
        2 7 5;...
        2 7 5;...
        NaN NaN NaN;...
        1 2 3;...
        2 6 7;...
        2 5 6;...
        2 6 5;...
        2 5 7;...
        ];
    bestContactRight = [...
        1 12 11;...
        NaN NaN NaN;... % best ring supposed to be 2
        NaN NaN NaN;...
        2 15 13;...
        NaN NaN NaN;...
        1 11 12;...
        2 14 13;...
        2 15 13;...
        1 11 10;...
        2 14 13;...
        2 13 14;...
        2 13 14;...
        1 10 12;...
        2 13 NaN;...
        2 15 14;...
        2 13 14;...
        1 10 11;...
        2 15 14;...
        2 14 15;...
        2 15 13;...
        2 15 13;...
        2 14 13;...
        2 15 13;...
        NaN NaN NaN;...
        2 15 13;...
        2 14 13;...
        1 12 10;...
        2 14 15;...
        2 13 14;...
        ];
    
    group = zeros(noPatients,2); % first column left, second column right
    groupCutOff = 0; % band for group assignment in mA
    reconstructionFileList = dir('..\leadReconstruction\*.mat');
    effectThresholdsRev02;
    sideEffectThresholdsRev02;
    
    for pIdx = 1:noPatients
        if pIdx ~= 7 % no CT coregistration for that patient
            load(['..\leadReconstruction\' reconstructionFileList(pIdx).name]);
            contactPositionLeft(pIdx,:,:) = reco.mni.coords_mm{1,2};
            contactPositionRight(pIdx,:,:) = reco.mni.coords_mm{1,1};
        end
    end
    
    % calculation of therapeutic window
    therapeuticWindow = sideEffectThreshold - effectThreshold;
    for pIdx = 1:noPatients
        if pIdx ~= 7 % no CT registration for that patient
            % left hemisphere
            if ~isnan(bestContactLeft(pIdx,1:2))
                % looking at raw therapeutic values and not looking at the
                % neurologic classification
                [mDir,idxDir] = max(therapeuticWindow(pIdx,2:7));
                bestContactLeft(pIdx,2) = idxDir+1;
                [~,idxWorstDir] = min(therapeuticWindow(pIdx,2:7));
                bestContactLeft(pIdx,3) = idxWorstDir+1;
                [mRing,idxRing] = max(therapeuticWindow(pIdx,17:18));
                bestContactLeft(pIdx,1) = idxRing;
                if mRing - mDir >= groupCutOff % Group A
                    group(pIdx,1) = -1;
                elseif mRing - mDir < -groupCutOff % Group B
                    group(pIdx,1) = 1;
                    % else remaining group 0
                end
            else
                group(pIdx,1) = NaN; % exclude
            end
            % right hemisphere
            if ~isnan(bestContactRight(pIdx,1:2))
                % looking at raw therapeutic values and not looking at the
                % neurologic classification
                [mDir,idxDir] = max(therapeuticWindow(pIdx,10:15));
                bestContactRight(pIdx,2) = idxDir+9;
                [~,idxWorstDir] = min(therapeuticWindow(pIdx,10:15));
                bestContactRight(pIdx,3) = idxWorstDir+9;
                [mRing,idxRing] = max(therapeuticWindow(pIdx,19:20));
                bestContactRight(pIdx,1) = idxRing;
                if mRing - mDir >= groupCutOff % Group A
                    group(pIdx,2) = -1;
                elseif mRing - mDir < -groupCutOff % Group B
                    group(pIdx,2) = 1;
                    % else remaining group 0
                end
            else
                group(pIdx,2) = NaN; % exclude
            end
        end
    end
    rhoDice = zeros(numberFolds,1); % for Spearman
    pValDice = zeros(numberFolds,1);
    rhoOverlapRatio = zeros(numberFolds,1); % for Spearman
    pvalOverlapRatio = zeros(numberFolds,1);
    rhoIntensity = zeros(numberFolds,1); % for Spearman
    pvalIntensity = zeros(numberFolds,1);
    figure(fh4); caxis([0 max(globalClinicalScore(:))])
    fhHotSpot = figure('color','w');

    voxelVolume = niiRight.voxsize(1) * niiRight.voxsize(2) * niiRight.voxsize(3);
end % kCrossValid == 1

%% plot hot spot
scoreThreshold = prctile(globalClinicalScore(globalClinicalScore>0),percentileCutOff);

figure(fhHotSpot);
hold off
% trisurf(boundarySTNLeft,ptCloud.Location((Idx & IdxLeft),1),ptCloud.Location((Idx & IdxLeft),2),ptCloud.Location((Idx & IdxLeft),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.2,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
hold on
trisurf(boundarySTNRight,ptCloud.Location((Idx & IdxRight),1),ptCloud.Location((Idx & IdxRight),2),ptCloud.Location((Idx & IdxRight),3),'Facecolor',[.4 .4 .4],'FaceAlpha',0.2,'LineStyle','none','EdgeColor',pcColors(colorIdx,:))
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
tmpIdx = find(globalClinicalScore>scoreThreshold);
[xx,yy,zz]=ind2sub(size(niiRight.img),tmpIdx); % find 3D-points that have correct value.
if ~isempty(xx)
    XYZ=[xx,yy,zz]; % concatenate points to one matrix.
    XYZ=map_coords_proxy(XYZ,niiRight); % map to mm-space
end
[boundaryZoneRight,~] = boundary(XYZ(:,1),XYZ(:,2),XYZ(:,3),0.25);
centerRight = mean(XYZ);
hHotSpot = trisurf(boundaryZoneRight,XYZ(:,1),XYZ(:,2),XYZ(:,3),'Facecolor',colorOrder(5,:),'FaceAlpha',0.8,'LineStyle','none');
view([-30,30])
xlim([0 20]); ylim([-20 -5]); zlim([-15 0])
title(sprintf('Hot spot defined by %d^{th} percentile and above',percentileCutOff))
if kCrossValid == 1
    view([-30 30])
    saveas(fhHotSpot,[baseFigureFolder 'hotSpotAK' num2str(kCrossValid)],'png')
    print(fhHotSpot,[baseFigureFolder 'hotSpotAK' num2str(kCrossValid)],'-depsc')
    title('')
    view([0 90])
    saveas(fhHotSpot,[baseFigureFolder 'hotSpotDK' num2str(kCrossValid)],'png')
    print(fhHotSpot,[baseFigureFolder 'hotSpotDK' num2str(kCrossValid)],'-depsc')    
end

%% overlap
idxHotSpot = find(globalClinicalScore>scoreThreshold);

debug = false;
disp('Testing')
tic
idxLeftPatientContactTesting = zeros(size(filesLeftTesting,1),2);
for fIdx=1:size(filesLeftTesting)
    tmp = strsplit(filesLeftTesting(fIdx).name,'K');
    pNumber = str2double(tmp{1}(end-2:end));
    if str2double(tmp{end}(1:end-4)) < 16
        cNumber = str2double(tmp{end}(1:end-4))-7; % -7 for conversion to neurology numbering
    elseif str2double(tmp{end}(1:end-4)) == 91011 % need to cut .nii at the end
        cNumber = 17;
    elseif str2double(tmp{end}(1:end-4)) == 121314
        cNumber = 18;
    end
    idxLeftPatientContactTesting(fIdx,:) = [pNumber cNumber];
end
idxRightPatientContactTesting = zeros(size(filesRightTesting,1),2);
for fIdx=1:size(filesRightTesting)
    tmp = strsplit(filesRightTesting(fIdx).name,'K');
    pNumber = str2double(tmp{1}(end-2:end));
    if str2double(tmp{end}(1:end-4)) < 8
        cNumber = str2double(tmp{end}(1:end-4))+9; % +9 for conversion to neurology numbering
    elseif str2double(tmp{end}(1:end-4)) == 123
        cNumber = 19;
    elseif str2double(tmp{end}(1:end-4)) == 456
        cNumber = 20;
    end
    idxRightPatientContactTesting(fIdx,:) = [pNumber cNumber];
end
overlapNumberVoxel = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
Dice = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
overlapRatio = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
ratioClinicalScore = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
sumClinicalScoreOverlap = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
sumClinicalScoreTotal = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
ratioClinicalScoreTotal = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
volumeTotal = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);

tmpClinicalScoreNormalized = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
tmpClinicalScoreNonNormalized = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);
tmpTherapeuticWindow = zeros(size(idxLeftPatientContactTesting,1)+size(idxRightPatientContactTesting,1),numberFolds);

for fIdx = 1:size(filesLeftTesting)
    VTADirection = ea_load_nii(['left/' filesLeftTesting(fIdx).name]);
    [xx,yy,zz] = ind2sub(size(VTADirection.img),find(VTADirection.img>0));
    XYZ=[xx,yy,zz]; % concatenate points to one matrix.
    XYZ=map_coords_proxy(XYZ,VTADirection); % map to mm-space
    XYZflipped = ea_flip_lr_nonlinear(XYZ); % flip mm coordinates from left to right
    tmpFlipped = map_proxy_coords(XYZflipped,niiRight);
    xxFlipped = tmpFlipped(:,1); yyFlipped = tmpFlipped(:,2); zzFlipped = tmpFlipped(:,3);
    
    idxVTA = sub2ind(size(niiRight.img),xxFlipped,yyFlipped,zzFlipped); % get the linear index for the base image
    overlapNumberVoxel(fIdx,kCrossValid) = sum(ismember(idxVTA,idxHotSpot));
    Dice(fIdx,kCrossValid) = 2*overlapNumberVoxel(fIdx,kCrossValid)/(length(idxVTA)+length(idxHotSpot));
    overlapRatio(fIdx,kCrossValid) = overlapNumberVoxel(fIdx,kCrossValid)/(length(idxVTA));
    overlapIndex = idxVTA(ismember(idxVTA,idxHotSpot));
    sumClinicalScoreOverlap(fIdx,kCrossValid) = sum(globalClinicalScore(overlapIndex));
    sumClinicalScoreTotal(fIdx,kCrossValid) = sum(globalClinicalScore(idxVTA));
    ratioClinicalScoreTotal(fIdx,kCrossValid) = sum(globalClinicalScore(idxVTA))/length(idxVTA);
    ratioClinicalScore(fIdx,kCrossValid) = sum(globalClinicalScore(overlapIndex))/sum(globalClinicalScore(idxVTA));
    volumeTotal(fIdx) = length(idxVTA)*voxelVolume;
end
for fIdx = 1:size(filesRightTesting)
    VTADirection = ea_load_nii(['right/' filesRightTesting(fIdx).name]);
    [xx,yy,zz] = ind2sub(size(VTADirection.img),find(VTADirection.img>0));
    XYZ=[xx,yy,zz]; % concatenate points to one matrix.
    XYZ=map_coords_proxy(XYZ,VTADirection); % map to mm-space
    XYZCorrected = map_proxy_coords(XYZ,niiRight); % map from mm space to index of base image
    xxCorrected = XYZCorrected(:,1); yyCorrected = XYZCorrected(:,2); zzCorrected = XYZCorrected(:,3);
    idxVTA = sub2ind(size(niiRight.img),xxCorrected,yyCorrected,zzCorrected); % get the linear index for the base image
    overlapNumberVoxel(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid) = sum(ismember(idxVTA,idxHotSpot));
    Dice(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid) = 2*overlapNumberVoxel(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid)/(length(idxVTA)+length(idxHotSpot));
    overlapRatio(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid) = overlapNumberVoxel(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid)/(length(idxVTA));
    overlapIndex = idxVTA(ismember(idxVTA,idxHotSpot));
    sumClinicalScoreOverlap(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid) = sum(globalClinicalScore(overlapIndex));
    sumClinicalScoreTotal(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid) = sum(globalClinicalScore(idxVTA));
    ratioClinicalScoreTotal(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid) = sum(globalClinicalScore(idxVTA))/length(idxVTA);
    ratioClinicalScore(fIdx+size(idxLeftPatientContactTesting,1),kCrossValid) = sum(globalClinicalScore(overlapIndex))/sum(globalClinicalScore(idxVTA));  
    volumeTotal(fIdx-2+size(idxLeftPatientContactTesting,1),kCrossValid) = length(idxVTA)*voxelVolume;
end

for k=1:size(idxLeftPatientContactTesting,1)
    tmpClinicalScoreNormalized(k,kCrossValid) = clinicalScoreNormalized(idxLeftPatientContactTesting(k,1),idxLeftPatientContactTesting(k,2));
    tmpClinicalScoreNonNormalized(k,kCrossValid) = clinicalScore(idxLeftPatientContactTesting(k,1),idxLeftPatientContactTesting(k,2));
    tmpTherapeuticWindow(k,kCrossValid) = therapeuticWindow(idxLeftPatientContactTesting(k,1),idxLeftPatientContactTesting(k,2));
end
for k=1:size(idxRightPatientContactTesting,1)
    tmpClinicalScoreNormalized(k+size(idxLeftPatientContactTesting,1),kCrossValid) = clinicalScoreNormalized(idxRightPatientContactTesting(k,1),idxRightPatientContactTesting(k,2));
    tmpClinicalScoreNonNormalized(k+size(idxLeftPatientContactTesting,1),kCrossValid) = clinicalScore(idxRightPatientContactTesting(k,1),idxRightPatientContactTesting(k,2));
    tmpTherapeuticWindow(k+size(idxLeftPatientContactTesting,1),kCrossValid) = therapeuticWindow(idxRightPatientContactTesting(k,1),idxRightPatientContactTesting(k,2));
end

tmpIdx = tmpClinicalScoreNormalized(:,kCrossValid)>0;
tmpIdxFullEffect = tmpClinicalScoreNonNormalized(:,kCrossValid)==1;

fhOverlap = figure('color','w');
plot(overlapRatio(tmpIdx,kCrossValid),tmpClinicalScoreNormalized(tmpIdx,kCrossValid),'o','MarkerFaceColor',[.4 .4 .4],'MarkerSize',4,'MarkerEdgeColor',[.4 .4 .4])
X = [ones(length(overlapRatio(tmpIdx,kCrossValid)),1) overlapRatio(tmpIdx,kCrossValid)];
xlabel('Overlap ratio')
ylabel('Clinical efficacy')
y = tmpClinicalScoreNormalized(tmpIdx,kCrossValid);
b = X\y;
yCalc = X*b;
hold on
plot(X(:,2), yCalc,'k-','linewidth',2)
[rhoOverlapRatio(kCrossValid),pvalOverlapRatio(kCrossValid)] = corr(overlapRatio(tmpIdx,kCrossValid),tmpClinicalScoreNormalized(tmpIdx,kCrossValid),'type','spearman');
title(sprintf('Spearman''s rho = %0.2f; p = %0.3f',rhoOverlapRatio(kCrossValid),pvalOverlapRatio(kCrossValid)))
xlim([0 1]); ylim([0 .7]);
saveas(fhOverlap,[baseFigureFolder 'overlapClinicalEfficacyK' num2str(kCrossValid)],'png')
print(fhOverlap,[baseFigureFolder 'overlapClinicalEfficacyK' num2str(kCrossValid)],'-depsc')
close(fhOverlap)

fhOverlap2 = figure('color','w');
plot(overlapRatio(tmpIdx,kCrossValid),tmpTherapeuticWindow(tmpIdx,kCrossValid),'o','MarkerFaceColor',[.4 .4 .4],'MarkerSize',4,'MarkerEdgeColor',[.4 .4 .4])
X = [ones(length(overlapRatio(tmpIdx,kCrossValid)),1) overlapRatio(tmpIdx,kCrossValid)];
xlabel('Overlap ratio')
ylabel('Therapeutic window (mA)')
y = tmpTherapeuticWindow(tmpIdx,kCrossValid);
b = X\y;
yCalc = X*b;
hold on
plot(X(:,2), yCalc,'k-','linewidth',2)
[rhoOverlapRatio2(kCrossValid),pvalOverlapRatio2(kCrossValid)] = corr(overlapRatio(tmpIdx,kCrossValid),tmpTherapeuticWindow(tmpIdx,kCrossValid),'type','spearman');
title(sprintf('Spearman''s rho = %0.2f; p = %0.3f',rhoOverlapRatio2(kCrossValid),pvalOverlapRatio2(kCrossValid)))
xlim([0 1]); ylim([0 5]);
saveas(fhOverlap2,[baseFigureFolder 'overlapTherapeuticWindowK' num2str(kCrossValid)],'png')
print(fhOverlap2,[baseFigureFolder 'overlapTherapeuticWindowK' num2str(kCrossValid)],'-depsc')
close(fhOverlap2)

fhIntensity = figure('color','w');
plot(ratioClinicalScoreTotal(tmpIdx,kCrossValid),tmpClinicalScoreNormalized(tmpIdx,kCrossValid),'o','MarkerFaceColor',[.4 .4 .4],'MarkerSize',4,'MarkerEdgeColor',[.4 .4 .4])
X = [ones(length(ratioClinicalScoreTotal(tmpIdx,kCrossValid)),1) ratioClinicalScoreTotal(tmpIdx,kCrossValid)];
xlabel('VTA intensity')
ylabel('Clinical efficacy')
y = tmpClinicalScoreNormalized(tmpIdx,kCrossValid);
b = X\y;
yCalc = X*b;
hold on
plot(X(:,2), yCalc,'k-','linewidth',2)
[rhoIntensity(kCrossValid),pvalIntensity(kCrossValid)] = corr(ratioClinicalScoreTotal(tmpIdx,kCrossValid),tmpClinicalScoreNormalized(tmpIdx,kCrossValid),'type','spearman');
title(sprintf('Spearman''s rho = %0.2f; p = %0.3f',rhoIntensity(kCrossValid),pvalIntensity(kCrossValid)))
xlim([-.1 .1]); ylim([0 0.7]);
saveas(fhIntensity,[baseFigureFolder 'intensityClinicalEfficacyK' num2str(kCrossValid)],'png')
print(fhIntensity,[baseFigureFolder 'intensityClinicalEfficacyK' num2str(kCrossValid)],'-depsc')
close(fhIntensity)

fhIntensity2 = figure('color','w');
plot(ratioClinicalScoreTotal(tmpIdx,kCrossValid),tmpTherapeuticWindow(tmpIdx,kCrossValid),'o','MarkerFaceColor',[.4 .4 .4],'MarkerSize',4,'MarkerEdgeColor',[.4 .4 .4])
X = [ones(length(ratioClinicalScoreTotal(tmpIdx,kCrossValid)),1) ratioClinicalScoreTotal(tmpIdx,kCrossValid)];
xlabel('VTA intensity')
ylabel('Therapeutic window (mA)')
y = tmpTherapeuticWindow(tmpIdx,kCrossValid);
b = X\y;
yCalc = X*b;
hold on
plot(X(:,2), yCalc,'k-','linewidth',2)
[rhoIntensity2(kCrossValid),pvalIntensity2(kCrossValid)] = corr(ratioClinicalScoreTotal(tmpIdx,kCrossValid),tmpTherapeuticWindow(tmpIdx,kCrossValid),'type','spearman');
title(sprintf('Spearman''s rho = %0.2f; p = %0.3f',rhoIntensity2(kCrossValid),pvalIntensity2(kCrossValid)))
xlim([-.1 .1]); ylim([0 5]);
saveas(fhIntensity2,[baseFigureFolder 'intensityTherapeuticWindowK' num2str(kCrossValid)],'png')
print(fhIntensity2,[baseFigureFolder 'intensityTherapeuticWindowK' num2str(kCrossValid)],'-depsc')
close(fhIntensity2)

toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% in-line functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function coords=map_coords_proxy(XYZ,V)

XYZ=[XYZ';ones(1,size(XYZ,1))];

coords=V.mat*XYZ;
coords=coords(1:3,:)';
end

function XYZ=map_proxy_coords(coords,V)

coords = [coords';ones(1,size(coords,1))];
XYZ = V.mat\coords;
XYZ = round(XYZ);
XYZ(XYZ<1) = 1;
XYZ = XYZ(1:3,:)';
end
