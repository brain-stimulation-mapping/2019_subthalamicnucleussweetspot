% June 2018, rev02 to include the ring level modes where contacts 234 and
% 567 are stimulated together
noPatients = 29;
noContacts = 10; % per lead
% conversion of Andreas ID to Khoa ID
conversionID = [12 10 13 9 23 2 6 7 4 14 5 1 11 15 3 8 16 17 18 29 19 20 25 21 22 26 27 28];
% columns 1-8 left contacts, columns 9-16 right contacts; then 17-20 are
% ring levels on left and right side

effectThreshold = zeros(noPatients,noContacts*2);
effectThreshold(conversionID(1),:) = [... % 
    3.5 3 3.5 2.5 4.25 2 4 4 2.5 3 3 3.5 2 4 3 2.5 2.5 2.5 2 1.5]; 
effectThreshold(conversionID(2),:) = [... % 
    5 4.75 5 5 4.5 6 5 6.5 4 4 4 4 4.75 3.5 4.25 4 5.5 6 4 4.5];
effectThreshold(conversionID(3),:) = [... % 
    3 3.5 2.75 2.5 4 2.5 3.5 4 3.5 3 3.5 5 4 4 4 4.5 2.5 3.5 3.5 4.25];
effectThreshold(conversionID(4),:) = [... % 
    2.5 2 2 2.5 2 1.5 1.5 1.5 4 3.5 2 2.5 4 2.5 3.5 3 1.5 1.5 2.5 2.5];
effectThreshold(conversionID(5),:) = [... % 
    4 5 5 6 5 6.5 6.5 4.5 1.5 2.5 2 2.5 3 2 2 2 4 4 2 1.5];
% effectThreshold(conversionID(6),:) = [... % no effect threshold
effectThreshold(conversionID(7),:) = [... % 
    4 2 2.5 3 3.5 3.5 3 4.5 4 2.5 1.5 2.5 1.5 2 1.5 2.5 3 2.5 2.5 3];
% effectThreshold(conversionID(8),:) = [... % skip, no good post-op CT
% effectThreshold(conversionID(9),:) = [... % no or zero effect threshold
effectThreshold(conversionID(10),:) = [... % 
    3.5 4 4 3.5 4 3 6 6 3 4.5 4 4 0 0 0 3.5 5 6.5 4 3];
% effectThreshold(conversionID(11),:) = [... % also all zero
effectThreshold(conversionID(12),:) = [... % 
    2.5 2.5 3.5 3 2.5 2.5 2 2 4.5 3 3.5 2.5 5 4 3 3.5 3 2 4 4.5];
effectThreshold(conversionID(13),:) = [... % tremor patient?
    4.5 5 4.5 5 3 4.5 3 5.5 3 4 3 2.5 1.5 3 2.5 3 5 2.5 3.5 2.5];
effectThreshold(conversionID(14),:) = [... % 
    3.6 2.5 3.0 2.0 2.0 3.5 1.5 4.0 2.5 3.5 3.0 2.0 3.5 2.0 2.0 8.0 2.5 2 2 2];
effectThreshold(conversionID(15),:) = [... % 
    3.0 3.0 2.5 2.5 3.0 3.0 2.5 3.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 3.5 2.5 0 0];
effectThreshold(conversionID(16),:) = [... % 
    4.0 6.0 5.5 4.5 5.0 5.0 4.5 5.5 4.5 6.0 5.0 5.5 7.0 6.5 5.5 4.0 6 5.5 6 7];
effectThreshold(conversionID(17),:) = [... % 
    4.5 6.0 5.5 5.5 8.0 7.0 7.5 6.0 4.0 4.5 4.0 4.5 2.5 3.5 3.0 3.5 6 7.5 4 2.5];
effectThreshold(conversionID(18),:) = [... % 
    5.0 3.5 2.0 3.5 3.5 2.5 5.0 5.0 3.5 2.0 2.0 2.0 2.5 2.0 0.0 3.0 2.5 3.5 2 2];
effectThreshold(conversionID(19),:) = [... % 
    3.5 5.5 2.0 3.5 2.0 2.0 1.5 3.5 4.5 5.0 3.5 3.5 4.0 4.0 2.0 3.0 3 2.5 4 2.5];
effectThreshold(conversionID(20),:) = [... % 
    5.0 6.5 5.5 5.0 4.5 4.5 4.5 3.5 5.5 4.0 3.5 4.0 2.5 3.0 2.5 3.5 6 4 3 2.5];
effectThreshold(conversionID(21),:) = [... % 
    3.5 4.5 3.5 4.5 3.0 3.5 2.5 4.5 3.5 2.5 3.5 3.0 3.0 2.5 3.5 2.0 4.5 4 1.5 1.5];
effectThreshold(conversionID(22),:) = [... % right side
    3.5 3.5 2.0 2.5 3.0 1.5 2.0 4.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 2.5 2.5 0 0];
effectThreshold(conversionID(23),:) = [... % 
    3.5 3.5 5.0 4.5 4.5 5.0 4.0 4.0 4.0 4.0 3.0 3.5 3.0 3.0 2.5 2.0 3 3.5 4.5 1.5];
effectThreshold(conversionID(24),:) = [... % 
    2.0 3.5 3.5 4.0 5.0 5.5 5.0 7.0 2.5 4.0 4.0 4.0 3.0 3.0 2.5 2.5 3.5 6 2.5 2.5];
effectThreshold(conversionID(25),:) = [... % 
    5.0 4.0 4.5 4.0 3.0 3.0 2.0 2.0 4.0 5.5 4.5 4.0 3.0 2.5 2.5 3.5 5.5 2 3.5 2.5];
effectThreshold(conversionID(26),:) = [... % 
    3.0 4.0 3.5 3.0 3.5 3.0 3.5 2.5 5.5 3.0 2.5 2.5 2.5 2.0 2.0 3.0 4 3 2.5 2];
effectThreshold(conversionID(27),:) = [... % 
    2.5 2.5 3.0 4.0 4.0 3.5 4.5 3.0 4.0 3.5 3.0 3.0 3.5 3.0 2.5 4.5 3 3.5 3 8];
effectThreshold(conversionID(28),:) = [... % 
    3.0 3.5 3.5 3.0 2.5 2.0 2.0 1.5 2.5 3.0 2.5 2.5 2.0 1.5 2.5 2.5 3.5 1.5 3 1.5];
