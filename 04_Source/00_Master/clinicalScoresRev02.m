% Rev 02 with ring level data 

noPatients = 29;
noContacts = 10; % per lead

% conversion of Andreas ID to Khoa ID
conversionID = [12 10 13 9 23 2 6 7 4 14 5 1 11 15 3 8 16 17 18 29 19 20 25 21 22 26 27 28];
rigorON = -1*ones(noPatients,noContacts*2); % zero meaning full effect, -1 exclude, >0 partial effect
rigorOFF = -1*ones(noPatients,noContacts*2); %

rigorON(conversionID(1),:) = [... % 
    1 0 0 0 0 0 0 1 1 0 0 0 0 1 0 0 0 0 0 0];
rigorON(conversionID(2),:) = [... % 
    5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5];
rigorON(conversionID(3),:) = [... % 
    0 0 0 0 0 0 0 1 1 0 0 0 1 0 0 1 0 0 0 0];
rigorON(conversionID(4),:) = [... % 
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
rigorON(conversionID(5),:) = [... % 
    1 0 1 0 1 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0];
rigorON(conversionID(6),:) = [... % 0 effect threshold
    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
rigorON(conversionID(7),:) = [... % 
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
rigorON(conversionID(8),:) = [... % no CT position data
    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
rigorON(conversionID(9),:) = [... % 0effect threshold
    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
rigorON(conversionID(10),:) = [... % 
    2 1 0 0 0 0 0 1 1 2 1 2 -1 -1 -1 0 0 0 0 0];
rigorON(conversionID(11),:) = [... % zero effect threshold
    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
rigorON(conversionID(12),:) = [... % 
    1 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0];
rigorON(conversionID(13),:) = [... % 
    3 1 1 1 0 0 0 0 2 2 2 1 0 0 0 0 1 0 2 0];
rigorON(conversionID(14),:) = [... % 
    2 0 0 0 0 0 0 1 1 2 1 2 1 2 1 0 0 0 0 0];
rigorON(conversionID(15),:) = [... % 
    2 0 1 0 0 1 0 2 -1 -1 -1 -1 -1 -1 -1 -1 0 0 -1 -1];
rigorON(conversionID(16),:) = [... % 
    4 3 3 4 1 1 1 1 4 3 3 3 0 0 0 0 3 1 3 0];
rigorON(conversionID(17),:) = [... % 
    2 1 1 1 1 0 1 0 1 0 0 0 0 0 0 0 1 0 0 0];
rigorON(conversionID(18),:) = [... % 
    1 0 0 0 0 0 0 1 0 0 0 0 0 0 -1 0 0 0 0 0];
rigorON(conversionID(19),:) = [... % 
    1 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0];
rigorON(conversionID(20),:) = [... % 
    2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0];
rigorON(conversionID(21),:) = [... % 
    3 2 0 0 0 0 0 0 3 1 1 1 1 1 1 1 1 0 1 1];
rigorON(conversionID(22),:) = [... % 
    2 0 0 0 1 0 0 0 -1 -1 -1 -1 -1 -1 -1 -1 0 0 -1 -1];
rigorON(conversionID(23),:) = [... % 
    1 0 1 0 0 0 0 1 2 2 1 1 2 1 0 2 0 0 1 0];
rigorON(conversionID(24),:) = [... % 
    2 2 2 2 2 2 0 0 2 2 0 0 0 0 0 0 2 0 0 0];
rigorON(conversionID(25),:) = [... % 
    1 2 1 2 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0];
rigorON(conversionID(26),:) = [... % 
    2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0];
rigorON(conversionID(27),:) = [... % 
    3 2 2 3 0 1 0 0 3 1 1 0 1 2 0 1 2 0 1 0];
rigorON(conversionID(28),:) = [... % 
    2 0 0 0 0 0 0 0 2 0 1 1 0 0 0 0 0 0 0 0];

%%
rigorOFF(conversionID(1),:) = [... % 
    3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3];
rigorOFF(conversionID(2),:) = [... % 
    5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5];
rigorOFF(conversionID(3),:) = [... % 
    3 3 3 3 3 3 3 3 2 2 2 2 2 2 2 2 3 3 2 2];
rigorOFF(conversionID(4),:) = [... % 
    1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 1 1 2 2];
rigorOFF(conversionID(5),:) = [... % 
    3 3 3 3 3 3 3 2 1 2 2 2 2 2 2 1 3 2 2 1];
rigorOFF(conversionID(6),:) = [... % effect threshold
    0 1 1 1 0 0 0 0 5 5 5 5 5 5 5 5 0 0 5 5]; % 
rigorOFF(conversionID(7),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(8),:) = [... % no CT position data
    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
rigorOFF(conversionID(9),:) = [... % 0effect threshold
    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
rigorOFF(conversionID(10),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(11),:) = [... % zero effect threshold
    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
rigorOFF(conversionID(12),:) = [... % 
    2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3 2 2 3 3];
rigorOFF(conversionID(13),:) = [... % 
    3 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(14),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(15),:) = [... % 
    3 3 3 3 3 3 3 3 -1 -1 -1 -1 -1 -1 -1 -1 3 3 -1 -1];
rigorOFF(conversionID(16),:) = [... % 
    4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4];
rigorOFF(conversionID(17),:) = [... % 
    2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 2 2 1 1];
rigorOFF(conversionID(18),:) = [... % 
    3 3 3 3 3 3 3 3 2 2 2 2 2 2 -1 2 3 3 2 2];
rigorOFF(conversionID(19),:) = [... % 
    1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];
rigorOFF(conversionID(20),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(21),:) = [... % 
    3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3];
rigorOFF(conversionID(22),:) = [... % 
    2 2 2 2 2 2 2 2 -1 -1 -1 -1 -1 -1 -1 -1 2 2 -1 -1];
rigorOFF(conversionID(23),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(24),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(25),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(26),:) = [... % 
    2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
rigorOFF(conversionID(27),:) = [... % 
    3 3 3 3 3 3 3 3 3 4 4 4 4 4 4 3 3 3 3 3];
rigorOFF(conversionID(28),:) = [... % 
    3 3 3 3 3 3 3 3 2 3 3 3 3 3 3 2 3 3 2 2];

%% clinicalScores
clinicalScore = (rigorOFF - rigorON)./rigorOFF; % multiply with 100 to have percent
clinicalScore(rigorON == -1) = 0;
clinicalScoreNormalized = clinicalScore;
tmpIdx = find(effectThreshold); % avoid division by zero
clinicalScoreNormalized(tmpIdx) = clinicalScoreNormalized(tmpIdx)./effectThreshold(tmpIdx);
