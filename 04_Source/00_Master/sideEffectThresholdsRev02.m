% Side effect thresholds
% June 2018, rev02 to include the ring level modes where contacts 234 and
% 567 are stimulated together

noPatients = 29;
noContacts = 10; % per lead
% conversion of Andreas ID to Khoa ID
conversionID = [12 10 13 9 23 2 6 7 4 14 5 1 11 15 3 8 16 17 18 29 19 20 25 21 22 26 27 28];
% columns 1-8 left contacts, columns 9-16 right contacts; then 17-20 are
% ring levels on left and right side

sideEffectThreshold = zeros(noPatients,noContacts*2);
sideEffectThreshold(conversionID(1),:) = [... % 
    3.5 4.5 4.5 4 4.25 5 4.5 5 3 3.5 3.5 4.5 4 4 5 4.5 4 5 4 4]; 
sideEffectThreshold(conversionID(2),:) = [... % 
    5 4.75 5 5 4.5 6 5 6.5 4 4 4 4 4.75 3.5 4.25 4 5.5 6 4 4.5];
sideEffectThreshold(conversionID(3),:) = [... % 
    4.5 5 4 4 4.5 4 4 4 5.5 3.5 4 5.5 4.75 4.5 5 6 4.5 4.5 5 5.5];
sideEffectThreshold(conversionID(4),:) = [... % 
    4 4 3.5 3.5 4 3.5 4 3.5 4 5 4 4.5 5.5 5 4.5 5 4 4 5.5 5];
sideEffectThreshold(conversionID(5),:) = [... % 
    4 6 5 4 4 4 5 5.5 3.5 4 3.5 4.5 4 4 5 5 3.5 5 4 4.5];
sideEffectThreshold(conversionID(6),:) = [... % no effect threshold
    3.5 4 4 5 4.5 4.5 5 4.5 3 4.5 4 4 4.5 4.5 5.5 5.5 3.5 4.5 4 5.5];
sideEffectThreshold(conversionID(7),:) = [... % 
    4 6 5 5 6.5 5.5 5 6 4 6 4.5 4.5 4.5 4.5 4.5 5.5 4.5 5 5 5];
% effectThreshold(conversionID(8),:) = [... % skip, no good post-op CT
sideEffectThreshold(conversionID(9),:) = [... % , no or zero effect threshold
    4 4.5 5.5 5.5 6.5 6.5 7 7 4 5 4 5 5.5 6 7 5.5 5.5 6 4.5 6];
sideEffectThreshold(conversionID(10),:) = [... % 
    3.5 4 4 3.5 4 3 4.5 6 3 4.5 4 4 4 -1 -1 5.5 4 5 4 5.5];
sideEffectThreshold(conversionID(11),:) = [... % also all zero
    4.5 4.5 4 3 4 4 4 5.5 3 3.5 3.5 4.5 3.5 4 6 4.5 5 5 3.5 5];
sideEffectThreshold(conversionID(12),:) = [... % 
    3.5 4 4.5 4.5 3.5 3.5 3 5.5 3.5 4 3.5 3.5 4 5 4 5.5 4 4 4 4];
sideEffectThreshold(conversionID(13),:) = [... % 
    3.5 5 3.5 3.5 5.5 5 4.5 5.5 2.5 3 2.5 2.5 4.5 4 4.5 3.5 4.5 5 2.5 4];
sideEffectThreshold(conversionID(14),:) = [... % 
    3.5 5.0 3.5 3.5 3.5 3.0 3.5 4.0 2.5 3.5 3.0 2.0 2.5 2.0 2.0 3.0 3.5 3.5 3.0 3.0];
sideEffectThreshold(conversionID(15),:) = [... % 
    3.0 3.0 2.5 2.5 3.5 2.5 3.5 3.0 8.0 8.0 8.0 8.0 8.0 8.0 8.0 8.0 3.0 3.0 8.0 3.5];
sideEffectThreshold(conversionID(16),:) = [... % 
    4.0 4.5 4.5 3.5 5.0 5.0 5.0 6.0 4.0 5.0 4.0 4.5 5.5 5.5 6.0 7.0 4.5 5.5 4.5 6.0];
sideEffectThreshold(conversionID(17),:) = [... % 
    4.0 5.0 4.5 4.5 7.0 6.5 7.0 7.5 2.5 2.5 2.5 3.0 3.5 3.5 5.0 3.5 5.0 6.5 3.0 4.5];
sideEffectThreshold(conversionID(18),:) = [... % 
    5.0 6.0 4.5 4.5 4.5 4.0 4.0 5.0 5.0 6.5 6.5 6.5 5.5 4.5 6.0 4.5 5.0 5.0 5.5 5.0];
sideEffectThreshold(conversionID(19),:) = [... % 
    5.0 6.0 6.5 5.5 4.0 4.0 6.5 4.0 4.5 5.0 3.5 5.5 5.0 4.0 5.5 4.5 6.5 5.0 4.0 4.5];
sideEffectThreshold(conversionID(20),:) = [... % 
    4.0 4.5 4.5 4.5 5.5 5.0 4.5 5.5 5.5 5.0 4.5 5.0 4.5 4.5 4.5 6.0 4.5 5.0 5.0 5.0];
sideEffectThreshold(conversionID(21),:) = [... % 
    3.5 4.0 5.0 4.5 5.0 5.5 6.0 6.5 3.5 4.0 3.5 4.0 5.0 5.5 7.0 5.5 4.5 5.0 4.5 5.5];
sideEffectThreshold(conversionID(22),:) = [... % right side 
    4.0 3.0 3.5 3.5 3.0 2.5 3.0 3.5 3.0 2.5 2.5 3.5 2.5 2.5 3.5 3.0  3.5 3.0 3.0 3.0];
sideEffectThreshold(conversionID(23),:) = [... % 
    3.5 4.5 5.0 4.0 5.0 6.0 5.5 4.0 4.0 4.0 3.0 3.5 3.0 3.0 4.5 2.0 4.0 4.0 4.5 3.5];
sideEffectThreshold(conversionID(24),:) = [... % 
    2.0 3.5 3.5 4.0 5.0 5.5 5.0 6.5 2.5 4.0 5.0 5.0 5.0 5.5 5.5 4.0 3.5 5.5 3.5 4.0];
sideEffectThreshold(conversionID(25),:) = [... % 
    4.5 4.0 4.5 4.0 3.5 4.0 3.5 3.5 3.5 5.0 4.0 4.0 3.5 3.5 3.0 5.0 4.0 3.5 4.5 4.5];
sideEffectThreshold(conversionID(26),:) = [... % 
    3.0 4.0 3.5 3.0 4.5 3.5 3.5 4.5 5.0 3.0 4.0 3.5 2.5 3.0 3.0 4.0 3.5 4.5 5.5 4.5];
sideEffectThreshold(conversionID(27),:) = [... % 
    2.5 3.5 3.0 4.0 4.5 5.5 5.0 6.5 4.0 5.0 4.0 4.0 4.5 3.0 5.0 6.0 4.0 5.0 5.5 5.5];
sideEffectThreshold(conversionID(28),:) = [... % 
    3.5 5.0 4.5 3.5 4.0 4.0 3.5 3.0 2.5 4.0 3.0 3.5 3.0 2.5 3.5 3.0 4.5 4.0 3.5 3.5];
