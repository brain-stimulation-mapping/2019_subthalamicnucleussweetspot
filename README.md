Nguyen et al. (2019), Directional stimulation of subthalamic nucleus sweet spot predicts clinical efficacy: Proof of concept. https://doi.org/10.1016/j.brs.2019.05.001 .

This repo has the analysis scripts only but not the Nifti files with the volumes of tissue activated. Please contact the main author, if you would like to have this. The analysis scripts are the final version used for the paper and the repo does not include the earlier revisions.
